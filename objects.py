import common
from sqlalchemy import Column, String, Integer, Numeric, Boolean

# Define all your tables (objects or views) here


class Service(common.Base):
    __tablename__ = 'services'

    address = Column('address', String(32), primary_key=True)
    registered = Column('registered', Boolean)

    def __init__(self, address, registered):
        self.address = address
        self.registered = registered
