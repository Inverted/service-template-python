import json
from flask import Flask
import common
import objects

# Connect to DB
common.Base.metadata.create_all(common.engine)
session = common.Session()

# Read service.json
with open('service.json') as json_file:
    service = json.load(json_file)

app = Flask(__name__)


@app.route('/')
def description():  # Required! Serve service.json at root
    return service


@app.route('/services')
def getServices():  # An example path: list all services
    services = session.query(objects.Service).all()
    returnObj = []
    for service in services:
        returnObj.append({'address': service.address})

    return json.dumps(returnObj)
