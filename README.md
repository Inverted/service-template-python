This project has the following dependencies:
```
flask
kafka-python
psycopg2
sqlalchemy
```
There should be two running applications:
1. `listener.py` should listen to Kafka events, and update the DB accordingly.
2. `app.py` should listen to HTTP requests, and respond accordingly.
Therefore, to run, run both `python listener.py` and `flask run`.

Feel free to use different packages or database backends (or programming language altogether), this is just an example project.
The only requirements are:
1. Serve a service.json at the root, which describes the HTTP paths this service can respond to. Use * as a wildcard.
2. Announce itself on the `service-registration` topic, with its (external) IP address as the key, with value 'true'. If you can, send a message containing 'false' on shutdown.
3. Allow environment variables `BROKERIP` for the Kafka broker IP and `ADDRESS` for its external address. This should ease deployment.