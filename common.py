from kafka import KafkaConsumer
from kafka import KafkaProducer
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import json
import os
import sys
config = {}
producer = {}
reader = {}


def checkSetting(key):
    if key not in config:
        envVal = os.environ.get(key)
        if envVal is None:
            sys.exit("Required config setting " + key + " not found")
        config[key] = envVal


try:
    cfgFile = "config_prod.json" if os.environ.get(
        'DEV_ENV') == "false" else "config_dev.json"
    with open(cfgFile) as json_file:
        config = json.load(json_file)
except:
    pass


checkSetting('BrokerIP')
checkSetting('Address')
checkSetting('DatabaseIP')

producer = KafkaProducer(bootstrap_servers=config['BrokerIP'])
consumer = KafkaConsumer(
    bootstrap_servers=config['BrokerIP'], auto_offset_reset='earliest')

engine = create_engine(config['DatabaseIP'])
Session = sessionmaker(bind=engine)
Base = declarative_base()
