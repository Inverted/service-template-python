import common
import objects

# Connect to DB
common.Base.metadata.create_all(common.engine)
session = common.Session()

# Required! Announce presence to API gateway
common.producer.send('service-registration', b'true',
                     common.config['Address'].encode('utf-8'))


# An example! Subscribe to service-registration topic
common.consumer.subscribe('service-registration')
for msg in common.consumer:
    serviceAddr = msg.key.decode('utf-8')
    isRegistered = msg.value.decode('utf-8') == 'true'

    # Create object
    service = objects.Service(serviceAddr, isRegistered)
    session.merge(service)  # Insert or update
    session.commit()  # Don't forget to actually commit the update


session.close()
